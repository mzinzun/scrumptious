from django.contrib import admin

# import the model you wish to register
from recipes.models import Recipe, RecipeStep, Ingredient

# Register your models here.
# admin.site.register(Recipe)
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        "title",
        "id",
    )
@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe_title",
        "order",
        "id",
    )
@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        "food_item",
        "id",
    )
