# Generated by Django 4.2.4 on 2023-08-28 21:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0003_rename_intructions_recipestep_instructions'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recipestep',
            old_name='instructions',
            new_name='steps',
        ),
    ]
