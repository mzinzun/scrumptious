from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required


# import Models to use in the view
from recipes.models import Recipe
from recipes.forms import RecipeForm

# Create your views here.

def edit_recipe(request,id):
    recipe = Recipe.objects.get(id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe",id=id)
    else:
        form = RecipeForm(instance=recipe)
        context = {
            "recipe": recipe,
            "form": form,
        }
        return render(request,"recipes/edit_recipe.html",context)


def create_recipe(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("recipe_list")
    else:
        form = RecipeForm()
        context = {
            "form": form,
        }
        return render(request,"recipes/create_recipe.html",context)


def show_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
        # optional without error catc
        # recipe = Recipe.objects.get(id=id)
    context = {
        "re": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipes": recipes,
    }
    return render(request,"recipes/list.html",context)

def delete_recipe(request,id):
    recipe = get_object_or_404(Recipe, id=id)
        # optional without error catch
        # recipe = Recipe.objects.get(id=id)
    recipe.delete()
    return redirect("recipe_list")

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list_all.html", context)
