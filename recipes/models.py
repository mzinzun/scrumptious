from django.db import models
from django.conf import settings

# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    # author = models.ForeignKey(
    # settings.AUTH_USER_MODEL,
    # RELATED_NAME = "recipes"
    # on_delete = models.CASCADE
    # null = True)

    # alternate way to display title in admin
    # def __str__(self):
    #     return self.title
    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name= 'recipes',
        on_delete=models.CASCADE,
        null =True,
    )
    def __str__(self):
        return self.title

class RecipeStep(models.Model):
    steps = models.TextField()
    order = models.PositiveIntegerField()
    recipe = models.ForeignKey(
        "Recipe",
        related_name="steps",
        on_delete=models.CASCADE
    )
    def recipe_title(self):
        return self.recipe.title

    class Meta:
        ordering = ['order']

class Ingredient(models.Model):
    amount = models.CharField(max_length=100)
    food_item = models.CharField(max_length=100)
    recipe = models.ForeignKey(
        "Recipe",
        related_name="food_item",
        on_delete=models.CASCADE
    )
    def recipe_title(self):
        return self.recipe.title

    class Meta:
        ordering = ['food_item']
