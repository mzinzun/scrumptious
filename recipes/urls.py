from django.urls import path
#import functions from view
from recipes.views import show_recipe,recipe_list, create_recipe, edit_recipe, delete_recipe, my_recipe_list

# Register view functions
urlpatterns = [
    path("<int:id>/",show_recipe, name = "show_recipe"),
    path("",recipe_list, name = "recipe_list"),
    path("create_recipe/", create_recipe, name="create_recipe"),
    path("edit_recipe/<int:id>", edit_recipe, name = "edit_recipe"),
    path("delete_recipe/<int:id>", delete_recipe, name = "delete_recipe"),
    path("list_all/",my_recipe_list, name = "my_recipe_list")
]
