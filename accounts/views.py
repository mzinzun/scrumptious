from django.shortcuts import render, redirect
from django.contrib.auth import login
from django.contrib.auth.models import User
from accounts.forms import SignUpForm, LogInForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def sign_up_form(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            password_confirmation = form.cleaned_data['password_confirmation']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                    first_name = first_name,
                    last_name= last_name,
                )
                return redirect("recipe_list")


    else:
        form = SignUpForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)

def login_form(request):
    if request.method == 'POST':
        form = LogInForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("recipe_list")
            else:
                return redirect("login_form")
    else:
        form = LogInForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("recipe_list")
