from django.urls import path
from accounts.views import sign_up_form, login_form, user_logout


# Register view functions
urlpatterns = [
    path("signup/",sign_up_form, name = "sign_up_form"),
    path("login/",login_form, name = "login_form"),
     path("logout/",user_logout, name = "user_logout"),
]
